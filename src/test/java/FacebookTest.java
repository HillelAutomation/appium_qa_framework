import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FacebookTest {

    private AppiumDriver driver;
    private WebDriverWait wait;
    private AppiumDriverLocalService service;
    private AppiumServiceBuilder builder;

    @Before
    public void setUpDriver() throws MalformedURLException {
        final DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.0");
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Moto X Style (XT1572)");
        desiredCapabilities.setCapability("appPackage", "ch.nzz.mobile");
        desiredCapabilities.setCapability("appActivity", "ch.nzz.nextgen.MainActivity");

        builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1");
        builder.usingPort(4723);
        builder.withCapabilities(desiredCapabilities);
        service = AppiumDriverLocalService.buildService(builder);
        service.start();

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 30, 300);
    }

    @Test
    public void testIncorrectFacebookLogin() throws MalformedURLException {
        driver.get("https://www.sportchek.ca/");
        WebElement searchBar = driver.findElement(By.xpath("//input[contains(@class, 'search__input')]"));
        searchBar.sendKeys("NIKE");

        List<WebElement> predictiveSearchDropdownItems = driver.findElementsByXPath("//div[@class='rfk_conwrapper']//li[@data-type='keyphrase']");
        Assert.assertTrue("There are no items displayed!", predictiveSearchDropdownItems.size() > 0);
        predictiveSearchDropdownItems.forEach(item ->
                Assert.assertTrue("There is incorrect item text present!",
                        StringUtils.contains(item.getText(), "NIKE")));
    }

    @Test
    public void testNzzNativeApplication() {

        MobileElement spinner = (MobileElement) driver.findElementByClassName("android.widget.ProgressBar");
        wait.until(ExpectedConditions.visibilityOf(spinner));
        wait.until(ExpectedConditions.invisibilityOf(spinner));

        MobileElement mainMenu = (MobileElement) driver.findElementByClassName("android.widget.ImageButton");
        mainMenu.click();

        MobileElement loginButton = (MobileElement) driver.findElementByXPath("//android.widget.TextView[@text='ANMELDEN']/parent::android.view.ViewGroup");
        wait.until(ExpectedConditions.visibilityOf(loginButton));
        loginButton.click();

        MobileElement loginInput = (MobileElement) driver.findElementByXPath("//android.widget.TextView[@text='E-Mail']");
        loginInput.setValue("incorrect@mail.ca");

        MobileElement passwordBox = (MobileElement) driver.findElementByXPath("//android.widget.TextView[@text='Passwort']");
        passwordBox.setValue("incorrectPassword");
    }

    @After
    public void tearDown() {
        service.stop();
    }
}
